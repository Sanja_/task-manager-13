package ru.karamyshev.taskmanager.util;

import ru.karamyshev.taskmanager.error.CommandIncorrectException;
import ru.karamyshev.taskmanager.error.IndexIncorrectException;

import java.util.Scanner;

public interface  TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        final String command = SCANNER.nextLine();
            try {
                return command;
            } catch (Exception e) {
                throw new CommandIncorrectException(command);
            }
    }

    static Integer nextNumber(){
        final String value = nextLine();
        try {
            return  Integer.parseInt(value);
        } catch (Exception e) {
        throw new IndexIncorrectException(value);
        }
    }
}
