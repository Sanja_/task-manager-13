package ru.karamyshev.taskmanager.api.repository;

import ru.karamyshev.taskmanager.model.Command;

public interface ICommandRepository {

    String[] getCommands(Command... values);

    String[] getArgs(Command... values);

    Command[] getTerminalCommands();

    String[] getCommands();

    String[] getArgs();
}
