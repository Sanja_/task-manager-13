package ru.karamyshev.taskmanager.error;

public class NameEmptyException extends RuntimeException {

    public NameEmptyException() {
        super("Error! Name is empty...");
    }
}
