package ru.karamyshev.taskmanager.error;

public class IdEmptyException extends RuntimeException {

    public IdEmptyException() {
        super("Error! Id is empty...");
    }
}
