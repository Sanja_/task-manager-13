package ru.karamyshev.taskmanager.error;

import ru.karamyshev.taskmanager.constant.MsgCommandConst;

public class CommandIncorrectException extends RuntimeException {

    public CommandIncorrectException(final String command) {
        super("Error! This command ``" +command+"`` is incorrect.");
    }
}
