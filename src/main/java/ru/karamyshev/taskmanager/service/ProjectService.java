package ru.karamyshev.taskmanager.service;

import ru.karamyshev.taskmanager.api.repository.IProjectRepository;
import ru.karamyshev.taskmanager.api.service.IProjectService;
import ru.karamyshev.taskmanager.error.IdEmptyException;
import ru.karamyshev.taskmanager.error.IndexIncorrectException;
import ru.karamyshev.taskmanager.error.NameEmptyException;
import ru.karamyshev.taskmanager.model.Project;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository iProjectRepository;

    public ProjectService( final IProjectRepository iProjectRepository) {
        this.iProjectRepository = iProjectRepository;
    }

    @Override
    public void create(final String name) {
        final Project project = new Project();
        project.setName(name);
        iProjectRepository.add(project);
    }

    @Override
    public void create(final String name, final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        iProjectRepository.add(project);
    }

    @Override
    public void add(final Project project) {
        if (project == null) return;
        iProjectRepository.add(project);
    }

    @Override
    public void remove(final Project project) {
        if (project == null) return;
        iProjectRepository.remove(project);
    }

    @Override
    public List<Project> findAll() {
        return iProjectRepository.findAll();
    }

    @Override
    public void clear() {
        iProjectRepository.clear();
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        if (index == null || index <= 0) throw new IndexIncorrectException();
        return iProjectRepository.findOneByIndex(index -1);
    }

    @Override
    public Project findOneByName(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return iProjectRepository.findOneByName(name);
    }

    @Override
    public Project updateProjectById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneById(id);
        if (project == null) return null;
        project.setId(Long.parseLong(id));
        project.setName(name);
        project.setDescription(description);
        return null;
    }

    @Override
    public Project removeOneByIndex(final Integer index) {
        if (index == null || index <= 0) throw new IndexIncorrectException();
        return iProjectRepository.removeOneByIndex(index -1);
    }

    @Override
    public Project removeOneByName(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return iProjectRepository.removeOneByName(name);
    }

    @Override
    public Project findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return iProjectRepository.findOneById(id);
    }

    @Override
    public Project removeOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return iProjectRepository.removeOneById(id);
    }

    @Override
    public Project updateProjectByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }
}
