package ru.karamyshev.taskmanager.repository;

import ru.karamyshev.taskmanager.api.repository.IProjectRepository;
import ru.karamyshev.taskmanager.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private List<Project> projects = new ArrayList<>();

    @Override
    public void add(final Project project){
        projects.add(project);
    }

    @Override
    public void remove(final Project project){
        projects.remove(project);
    }

    @Override
    public List<Project> findAll(){
        return projects;
    }

    @Override
    public void clear(){
        projects.clear();
    }

    @Override
    public Project findOneById(final String id) {
        for (final Project task: projects){
            if (Long.parseLong(id) == task.getId()) return task;
        }
        return null;
    }

    @Override
    public Project removeOneById(final String id) {
        final Project task = findOneById(id);
        if (task == null) return null;
        projects.remove(task);
        return task;
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        final int arraySize = projects.size() -1;
        if (arraySize < index) return null;
        return projects.get(index);
    }

    @Override
    public Project findOneByName(final String name) {
        for (final Project project: projects){
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project removeOneByIndex(final Integer index) {
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        remove(project);
        return project;
    }

    @Override
    public Project removeOneByName(final String name) {
        final Project project = findOneByName(name);
        if (project == null) return null;
        remove(project);
        return project;
    }
}
